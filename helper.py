import numpy as np
import torch
import k3d

from clearml import Dataset, Task, Logger

from scipy.spatial.transform import Rotation


class CombineFeatures:
    def __call__(self, data):
        data.x = torch.cat((data.pos, data.norm), dim=1)
        data.class_feature = torch.zeros(32)
        class_id = (int(data.crown_type[1]) - 1) * 8 + (int(data.crown_type[2]) - 1)
        data.class_feature[class_id] = 1.0
        return data


class SetTarget:
    def __call__(self, data):
        data.y = data.quaternion
        return data


class ClearExtra:
    def __call__(self, data):
        data.axes = None
        data.quaternion = None
        data.client_id = None
        data.crown_type = None
        return data


def render_sample(plot, sample, *args, axes=True):
    plot += k3d.mesh(
        vertices=np.array(sample.pos, dtype=np.float32),
        indices=np.array(sample.face).T,
        shader='flat',
        opacity=1.,
        color=0xf0f0f0
    )
    center = torch.mean(sample.pos, dim=0).numpy()
    if axes:
        colors = [0xff0000, 0x0000ff, 0x00ff00]
        names = ['x', 'y', 'z']
        for i in range(3):
            offset = sample.axes[i].numpy()*1.5
            plot += k3d.line([center, center + offset], color=colors[i], width=0.04)
            plot += k3d.texture_text(names[i], position=center + offset * 1.1, color=colors[i], size=0.2)
    for obj in args:
        plot += obj
    return plot


def predict_rotation(sample, model, device):
    model.eval()
    sample = sample.to(device)
    quat = model(sample).to('cpu').detach().numpy()
    return Rotation.from_quat(quat)


def np_angle(x, y):
    x /= np.linalg.norm(x)
    y /= np.linalg.norm(y)
    return np.arccos(np.clip(np.dot(x, y), -1, 1)) / np.pi * 180


def angle(sample, model, device):
    model.eval()
    sample = sample.to(device)
    quat_predicted = model(sample).to('cpu').detach().numpy()
    quat_target = sample.y.to('cpu').detach().numpy()
    z_predicted = Rotation.from_quat(quat_predicted).apply(np.array([0, 0, 1]))
    z_target = Rotation.from_quat(quat_target).apply(np.array([0, 0, 1]))
    return np_angle(z_predicted, z_target)


def evaluate(loader, model, loss, writer, device, epoch, name="test"):
    model.eval()
    total_angle = []
    test_loss = 0
    with torch.no_grad():
        for sample in loader:
            sample = sample.to(device)
            output = model(sample)
            test_loss += loss(output.reshape(-1, 4), sample.y.reshape(-1, 4)).item()
            quat_predicted = output.to('cpu').detach().numpy()
            quat_target = sample.y.to('cpu').detach().numpy()
            z_predicted = Rotation.from_quat(quat_predicted).apply(np.array([0, 0, 1]))
            z_target = Rotation.from_quat(quat_target).apply(np.array([0, 0, 1]))
            total_angle.append(np_angle(z_predicted, z_target))

    total_angle = np.array(total_angle)
    test_loss /= len(loader.dataset)
    test_mean = np.mean(total_angle)
    test_std = np.std(total_angle)
    test_median = np.median(total_angle)
    test_max = np.max(total_angle)
    test_min = np.min(total_angle)
    Logger.current_logger().report_scalar(
        "{}".format(name), "loss", iteration=epoch, value=test_loss)
    Logger.current_logger().report_scalar(
        "{}".format(name), "Mean", iteration=epoch, value=test_mean)
    Logger.current_logger().report_scalar(
        "{}".format(name), "Std", iteration=epoch, value=test_std)
    Logger.current_logger().report_scalar(
        "{}".format(name), "Median", iteration=epoch, value=test_median)
    Logger.current_logger().report_scalar(
        "{}".format(name), "Max", iteration=epoch, value=test_max)
    Logger.current_logger().report_scalar(
        "{}".format(name), "Min", iteration=epoch, value=test_min)
    print('\n{} set: Average loss: {:.4f}, Mean: {:.4f}, Std: {:.4f}\n'.format(
        name, test_loss, test_mean, test_std))
    print('{} set: Median: {:.4f}, Max: {:.4f}, Min: {:.4f}\n'.format(
        name, test_median, test_max, test_min))

    writer.add_scalar("Angle/{}".format(name), test_mean)


def train(model, loss, loader, writer, device, optimizer, epoch, log_interval):
    tloader = enumerate(loader)
    model.train()
    for batch_idx, data in tloader:
        data = data.to(device)

        optimizer.zero_grad()
        out = model(data)
        loss_result = loss(out.reshape(-1, 4), data.y.reshape(-1, 4))
        loss_result.backward()
        optimizer.step()

        if batch_idx % log_interval == 0:
            Logger.current_logger().report_scalar(
                "train", "loss", iteration=(epoch * len(loader) + batch_idx), value=loss_result.item())
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * loader.batch_size, len(loader.dataset),
                       100. * batch_idx / len(loader), loss_result.item()))

        writer.add_scalar("Loss/train", loss_result.item(), batch_idx)


def prepare_data(dataset_name):
    task = Task.current_task()

    try:
        dataset = Dataset.get(dataset_project=task.get_project_name(), dataset_name=dataset_name)
    except ValueError:
        dataset = None

    dataset_folder = dataset.get_local_copy()
    return dataset_folder
