# Code based on an example from pytorch_geometric
# https://github.com/rusty1s/pytorch_geometric/blob/master/examples/pointnet2_classification.py

import torch
import torch.nn.functional as F
from torch.nn import Sequential as Seq, Linear as Lin, ReLU, BatchNorm1d as BN
from torch_geometric.nn import PointConv, fps, radius, global_max_pool

class SAModule(torch.nn.Module):
    def __init__(self, ratio, r, nn):
        super(SAModule, self).__init__()
        self.ratio = ratio
        self.r = r
        self.conv = PointConv(nn)

    def forward(self, x, pos, batch):
        idx = fps(pos, batch, ratio=self.ratio)
        row, col = radius(pos, pos[idx], self.r, batch, batch[idx],
                          max_num_neighbors=64)
        edge_index = torch.stack([col, row], dim=0)
        x = self.conv(x, (pos, pos[idx]), edge_index)
        pos, batch = pos[idx], batch[idx]
        return x, pos, batch

class GlobalSAModule(torch.nn.Module):
    def __init__(self, nn):
        super(GlobalSAModule, self).__init__()
        self.nn = nn

    def forward(self, x, pos, batch):
        x = self.nn(torch.cat([x, pos], dim=1))
        x = global_max_pool(x, batch)
        pos = pos.new_zeros((x.size(0), 3))
        batch = torch.arange(x.size(0), device=batch.device)
        return x, pos, batch

def MLP(channels, batch_norm=True):
    return Seq(*[
        Seq(Lin(channels[i - 1], channels[i]), ReLU(), BN(channels[i]))
        for i in range(1, len(channels))
    ])

def convert_Avec_to_A(A_vec):
    """ Convert BxM tensor to BxNxN symmetric matrices """
    """ M = N*(N+1)/2"""
    if A_vec.dim() < 2:
        A_vec = A_vec.unsqueeze(dim=0)
    
    if A_vec.shape[1] == 10:
        A_dim = 4
    elif A_vec.shape[1] == 55:
        A_dim = 10
    else:
        raise ValueError("Arbitrary A_vec not yet implemented")

    idx = torch.triu_indices(A_dim,A_dim)
    A = A_vec.new_zeros((A_vec.shape[0],A_dim,A_dim))   
    A[:, idx[0], idx[1]] = A_vec
    A[:, idx[1], idx[0]] = A_vec
    return A.squeeze()

def A_vec_to_quat(A_vec):
    A = convert_Avec_to_A(A_vec)
    A = A.reshape(-1, 4, 4)
    _, evs = torch.symeig(A, eigenvectors=True)
    return evs[:,:,0].squeeze()

class RotationPointNet(torch.nn.Module):
    def __init__(self):
        super(RotationPointNet, self).__init__()

        self.sa1_module = SAModule(0.5, 0.2, MLP([6 + 3, 64, 128]))
        self.sa2_module = SAModule(0.25, 0.4, MLP([128 + 3, 256, 256]))
        self.sa3_module = GlobalSAModule(MLP([256 + 3, 512, 256]))

        self.lin1 = Lin(256 + 32, 128)
        self.lin2 = Lin(128, 64)
        self.lin3 = Lin(64, 10)    
        
    def forward(self, data):
        try:
            batch = data.batch
        except:
            batch = torch.zeros(data.pos.shape[0], dtype=torch.long).to(data.pos.device)
        sa0_out = (data.x, data.pos, batch)
        sa1_out = self.sa1_module(*sa0_out)
        sa2_out = self.sa2_module(*sa1_out)
        sa3_out = self.sa3_module(*sa2_out)
        x, pos, batch = sa3_out

        x = torch.cat([x, data.class_feature.reshape(-1, 32)], dim=1)
        x = F.relu(self.lin1(x))
        x = F.relu(self.lin2(x))
        x = self.lin3(x)
        q = A_vec_to_quat(x.reshape(-1, 10))
        return q
