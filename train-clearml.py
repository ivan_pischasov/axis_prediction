from __future__ import print_function
import json
import argparse
from copy import deepcopy
from pathlib import Path
import os
from tempfile import gettempdir

from helper import *
from clearml import Task, Logger

import numpy as np
import torch

from torchvision.transforms import Compose
from torch_geometric.transforms import NormalizeScale, SamplePoints
from crown_dataset import CrownDataset
from torch_geometric.data import DataLoader

from rotation_pointnet import RotationPointNet
from torch.utils.tensorboard import SummaryWriter
from torch.nn import CosineEmbeddingLoss, MSELoss
import torch.nn.functional as F

import k3d


def parse_args():
    parser = argparse.ArgumentParser(description='Train a Rotation Pointnet.')

    parser.add_argument('--project_name', help='ClearML project name', default='Axis_prediction')
    parser.add_argument('--task_name', help='ClearML task name', default='rotation_pointnet test')
    parser.add_argument('--queue', help='If set, the task is scheduled for remote execution in specified queue')
    parser.add_argument('--model_version', help='Model version', default='test')
    parser.add_argument('--model_name', help='Model name', default='rotation_pointnet')
    parser.add_argument('--model_path', help='Model name', default='.')
    parser.add_argument('--dataset_name', help='Name of clearml.Dataset for training.', default='Axis_base_v_0.2')
    parser.add_argument('--docker', help='Select the docker image to be executed in by the remote session',
                        default='pytam:clearml')

    parser.add_argument('--preload', action='store_true', help='Preload dataset via all data transformers')
    parser.add_argument(
        '--optimizer',
        choices=['adam', 'sgd'],
        default='adam',
        help='Optimizer')
    parser.add_argument(
        '--loss',
        choices=['quat', 'cosine', 'mse', 'angle'],
        default='quat',
        help='Loss function')
    parser.add_argument('--lr', help='Learning rate', type=float, default=1e-3, metavar='LR')
    parser.add_argument('--momentum', help='Momentum parameter', type=float, default=0.99, metavar='M')
    parser.add_argument('--weight_decay', help='Weight decay parameter', type=float, default=1e-8)

    parser.add_argument('--epochs', help='Number of epochs', type=int, default=4, metavar='N')
    parser.add_argument('--log_interval', type=int, default=10, metavar='N',
                        help='How many batches to wait before logging training status')
    parser.add_argument('--batch_size', help='Batch size', type=int, default=3, metavar='N')
    parser.add_argument('--test_batch_size', help='Test batch size', type=int, default=1, metavar='N')
    parser.add_argument('--num_workers', help='Number of dataloader workers', type=int, default=3, metavar='N')
    parser.add_argument('--num_models', help='Number of teeth models to limit', type=int, default=-1)
    parser.add_argument('--num_points', help='Number of points in teeth model', type=int, default=3000)
    parser.add_argument('--train_all', action='store_true', help='Add validation to train')

    args = parser.parse_args()
    return args


def train_rotation_pointnet_model(cfg):

    data_dir = Path(prepare_data(cfg['dataset_name']))

    pretransforms = [
        NormalizeScale(),
        SetTarget(),
    ]

    transforms = [
        SamplePoints(cfg['num_points'], include_normals=True),
        #    RandomRotation(numpy.pi * 0.05),
        CombineFeatures(),
        ClearExtra(),
    ]

    with open(data_dir / 'train_config.json') as f:
        case_split = json.load(f)

    dataset_options = {
        'preload': cfg['preload'],
        'num_models': cfg['num_models'],
        'pretransform': Compose(pretransforms),
        'transform': Compose(transforms)
    }
    train_cases = case_split['train']
    if cfg['train_all']:
        train_cases += case_split['validation']
    train_set = CrownDataset(data_dir, train_cases,
                             **dataset_options
                             )
    test_set = CrownDataset(data_dir, case_split['test'],
                            **dataset_options
                            )
    validation_set = CrownDataset(data_dir, case_split['validation'],
                                  **dataset_options
                                  )
    train_loader = DataLoader(train_set, shuffle=True, batch_size=cfg['batch_size'], num_workers=cfg['num_workers'])
    test_loader = DataLoader(test_set, shuffle=False, batch_size=cfg['test_batch_size'], num_workers=cfg['num_workers'])
    validation_loader = DataLoader(validation_set, shuffle=False, batch_size=cfg['test_batch_size'], num_workers=cfg['num_workers'])

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    model = RotationPointNet()

    model.to(device)

    if cfg['loss'] == 'cosine':
        _loss = CosineEmbeddingLoss()
        loss = lambda x, y: _loss(x, y, torch.ones(x.shape[0]).to(device))
    elif cfg['loss'] == 'mse':
        loss = MSELoss()
    elif cfg['loss'] == 'angle':
        def tensor_cos(x, y):
            return torch.sum(F.normalize(x) * F.normalize(y), dim=1)

        def tensor_acos(x):
            return torch.acos(torch.clamp(tensor_cos(x, y), -1, 1)) / np.pi * 180

        loss = lambda x, y: torch.mean(tensor_cos(tensor_cos(x, y)) ** 2)
    elif cfg['loss'] == 'quat':
        def quat_norm_diff(q_a, q_b):
            assert (q_a.shape == q_b.shape)
            assert (q_a.shape[-1] == 4)
            if q_a.dim() < 2:
                q_a = q_a.unsqueeze(0)
                q_b = q_b.unsqueeze(0)
            return torch.min((q_a - q_b).norm(dim=1), (q_a + q_b).norm(dim=1)).squeeze()

        def quat_chordal_squared_loss(q, q_target, reduce=True):
            assert (q.shape == q_target.shape)
            d = quat_norm_diff(q, q_target)
            losses = 2 * d * d * (4. - d * d)
            loss = losses.mean() if reduce else losses
            return loss

        loss = lambda x, y: quat_chordal_squared_loss(x, y)
    else:
        raise ValueError()

    if cfg['optimizer'] == 'adam':
        optimizer = torch.optim.Adam(model.parameters(), lr=cfg['lr'], weight_decay=cfg['weight_decay'])
    elif cfg['optimizer'] == 'sgd':
        optimizer = torch.optim.SGD(model.parameters(), cfg['lr'], cfg['momentum'])
    else:
        raise ValueError()

    writer = SummaryWriter()
    for epoch in range(1, cfg['epochs']+1):
        train(
            model=model,
            loss=loss,
            loader=train_loader,
            writer=writer,
            device=device,
            optimizer=optimizer,
            epoch=epoch,
            log_interval=cfg['log_interval']
        )
        torch.save(model.state_dict(), cfg['model_path']+'_{}.pt'.format(epoch))
        evaluate(validation_loader, model, loss, writer, device, epoch, name="validation")

    torch.save(model.state_dict(), cfg['model_path']+'_final.pt')
    evaluate(test_loader, model, loss, writer, device, cfg['epochs']+1)


if __name__ == '__main__':

    args = parse_args()
    if args.queue:
        args.model_path = os.path.join(gettempdir(), '{}_{}'.format(args.model_name, args.model_version))
    else:
        args.model_path += '/{}_{}'.format(args.model_name, args.model_version)

    config = {
        'model_path': args.model_path,
        'dataset_name': args.dataset_name,
        'log_interval': args.log_interval,
        'num_workers': args.num_workers,
        'preload': args.preload,
        'optimizer': args.optimizer,
        'loss': args.loss,
        'lr': args.lr,
        'momentum': args.momentum,
        'weight_decay': args.weight_decay,
        'epochs': args.epochs,
        'batch_size': args.batch_size,
        'test_batch_size': args.test_batch_size,
        'num_models': args.num_models,
        'num_points': args.num_points,
        'train_all': args.train_all
    }

    Task.add_requirements('./requirements.txt')
    Task.ignore_requirements('MinkowskiEngine')

    task = Task.init(
        project_name=args.project_name,
        task_name=args.task_name,
        auto_connect_frameworks=True,
        auto_connect_arg_parser=False,
        auto_resource_monitoring=True
    )
    # Schedule for remote execution
    if args.queue:
        task.execute_remotely(queue_name=args.queue)

    train_rotation_pointnet_model(config)
