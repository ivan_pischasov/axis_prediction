from types import SimpleNamespace as Object
from torch_geometric.data import Dataset, Data
from copy import deepcopy
from pathlib import Path
import numpy as np
import torch
from tqdm.notebook import tqdm

class CrownDataset(Dataset):
    
    def __init__(self, data_path: Path, ids, num_models: int = 1, preload = False, pretransform = None, transform = None):
        self.pretransform = pretransform
        self.transform = transform
        self.preload = preload
        self.data_path = data_path
        
        # # Limit to num_models items
        # if num_models == -1:
        #     num_models = len(ids)
        # num_models = min(num_models, len(ids))
        # ids = np.random.choice(ids, num_models)
        
        self.data = ids
        
        if self.preload:
            self.data = [self.load(id) for id in tqdm(self.data)]
            if self.pretransform is not None:
                self.data = [self.pretransform(sample) for sample in self.data]
        
    def load(self, id):
        loaded = np.load(self.data_path / (id + '_data.npz'))
        sample = Data(
            pos = torch.tensor(loaded['vertices'], dtype=torch.float32),
            face = torch.tensor(loaded['faces'], dtype=torch.long).transpose(0, 1),
            axes = torch.tensor(loaded['axes'], dtype=torch.float32),
            quaternion = torch.tensor(loaded['quaternion'], dtype=torch.float32),
            client_id = id.split('_')[0],
            crown_type = id.split('_')[-1]
        )
        return sample
    
    def __len__(self):
        return len(self.data)
    
    def get_random_no_transform(self):
        sample = np.random.choice(self.data)
        if not self.preload:
            sample = self.load(sample)
        return sample
    
    def __getitem__(self, idx):
        sample = self.data[idx]
        if not self.preload:
            sample = self.load(sample)
            if self.pretransform is not None:
                sample = self.pretransform(sample)
        if self.transform is not None:
            if self.preload:
                sample = deepcopy(sample)
            sample = self.transform(sample)
        return sample